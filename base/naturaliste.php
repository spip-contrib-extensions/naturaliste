<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Naturaliste
 * @copyright  2020
 * @author     Eric Lupinacci
 * @licence    GNU/GPL
 * @package    SPIP\Naturaliste\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function naturaliste_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['taxonviews'] = 'taxonviews';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function naturaliste_declarer_tables_objets_sql($tables) {

	$tables['spip_taxonviews'] = array(
		'type' => 'taxonview',
		'principale' => 'oui',
		'field'=> array(
			'id_taxonview'       => 'bigint(21) NOT NULL',
			'uid_naturaliste'    => 'bigint(21) NOT NULL',
			'url_naturaliste'    => 'text DEFAULT "" NOT NULL',
			'tsn'                => 'bigint(21) NOT NULL',
			'login_observateur'  => 'varchar(255) DEFAULT "" NOT NULL',
			'nom_observateur'    => 'text DEFAULT "" NOT NULL',
			'id_auteur'          => 'bigint(21) NOT NULL default 0',
			'latitude_lieu'      => 'double NOT NULL',
			'longitude_lieu'     => 'double NOT NULL',
			'id_lieu'            => 'bigint(21) NOT NULL default 0',
			'nom_lieu'           => 'text DEFAULT "" NOT NULL',
			'date_observation'   => 'datetime DEFAULT "0000-00-00 00:00:00" NOT NULL',
			'date'               => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'statut'             => 'varchar(20)  DEFAULT "0" NOT NULL',
			'maj'                => 'TIMESTAMP NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_taxonview',
			'KEY tsn'         => 'tsn',
		),
		'titre' => '"" AS titre, "" AS lang',
		'date' => 'date',
		'champs_editables'  => array('id_auteur', 'id_lieu', 'nom_lieu'),
		'champs_versionnes' => array('nom_lieu'),
		'rechercher_champs' => array("nom_lieu" => 2),
		'tables_jointures'  => array('spip_taxonviews_liens'),
		'statut_textes_instituer' => array(
			'prepa'    => 'texte_statut_en_cours_redaction',
			'prop'     => 'texte_statut_propose_evaluation',
			'publie'   => 'texte_statut_publie',
			'refuse'   => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		),
		'statut'=> array(
			array(
				'champ'     => 'statut',
				'publie'    => 'publie',
				'previsu'   => 'publie,prop,prepa',
				'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'texte_changer_statut' => 'taxonview:texte_changer_statut_taxonview',


	);

	return $tables;
}


/**
 * Déclaration des tables secondaires (liaisons)
 *
 * @pipeline declarer_tables_auxiliaires
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function naturaliste_declarer_tables_auxiliaires($tables) {

	$tables['spip_taxonviews_liens'] = array(
		'field' => array(
			'id_taxonview'       => 'bigint(21) DEFAULT "0" NOT NULL',
			'id_objet'           => 'bigint(21) DEFAULT "0" NOT NULL',
			'objet'              => 'VARCHAR(25) DEFAULT "" NOT NULL',
			'vu'                 => 'VARCHAR(6) DEFAULT "non" NOT NULL',
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_taxonview,id_objet,objet',
			'KEY id_taxonview'   => 'id_taxonview',
		)
	);

	return $tables;
}
