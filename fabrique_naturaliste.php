<?php

/**
 *  Fichier généré par la Fabrique de plugin v7
 *   le 2020-11-01 16:13:15
 *
 *  Ce fichier de sauvegarde peut servir à recréer
 *  votre plugin avec le plugin «Fabrique» qui a servi à le créer.
 *
 *  Bien évidemment, les modifications apportées ultérieurement
 *  par vos soins dans le code de ce plugin généré
 *  NE SERONT PAS connues du plugin «Fabrique» et ne pourront pas
 *  être recréées par lui !
 *
 *  La «Fabrique» ne pourra que régénerer le code de base du plugin
 *  avec les informations dont il dispose.
 *
**/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$data = array (
  'fabrique' => 
  array (
    'version' => 7,
  ),
  'paquet' => 
  array (
    'prefixe' => 'naturaliste',
    'nom' => 'Naturaliste',
    'slogan' => 'Observer le monde vivant',
    'description' => 'Ce plugin permet de récupérer les observations d\'espèces du monde vivant enregistrées sur la plate-forme iNaturalist et de lier ces observations aux taxons fournis par le plugin Taxonomie.',
    'credits' => 
    array (
      'logo' => 
      array (
        'texte' => '',
        'url' => '',
      ),
    ),
    'version' => '1.0.0',
    'auteur' => 'Eric Lupinacci',
    'auteur_lien' => 'https://blog.smellup.net',
    'licence' => 'GNU/GPL',
    'categorie' => 'divers',
    'etat' => 'dev',
    'compatibilite' => '[3.2.0;3.3.*]',
    'documentation' => '',
    'administrations' => 'on',
    'schema' => '1',
    'formulaire_config' => 'on',
    'formulaire_config_titre' => 'Configurer le plugin Naturaliste',
    'fichiers' => 
    array (
      0 => 'autorisations',
      1 => 'fonctions',
      2 => 'pipelines',
    ),
    'inserer' => 
    array (
      'paquet' => '',
      'administrations' => 
      array (
        'maj' => '',
        'desinstallation' => '',
        'fin' => '',
      ),
      'base' => 
      array (
        'tables' => 
        array (
          'fin' => '',
        ),
      ),
    ),
    'scripts' => 
    array (
      'pre_copie' => '',
      'post_creation' => '',
    ),
    'exemples' => '',
  ),
  'objets' => 
  array (
    0 => 
    array (
      'nom' => 'Observations taxonomiques',
      'nom_singulier' => 'Observation taxonomique',
      'genre' => 'feminin',
      'logo' => 
      array (
        0 => '',
        32 => '',
        24 => '',
        16 => '',
        12 => '',
      ),
      'table' => 'spip_taxonviews',
      'cle_primaire' => 'id_taxonview',
      'cle_primaire_sql' => 'bigint(21) NOT NULL',
      'table_type' => 'taxonview',
      'champs' => 
      array (
        0 => 
        array (
          'nom' => 'Identifiant iNaturalist',
          'champ' => 'uid_naturaliste',
          'sql' => 'bigint(21) NOT NULL',
          'caracteristiques' => 
          array (
            0 => 'obligatoire',
          ),
          'recherche' => '',
          'saisie' => '',
          'explication' => '',
          'saisie_options' => '',
        ),
        1 => 
        array (
          'nom' => 'URL iNaturalist de l\'observation',
          'champ' => 'url_naturaliste',
          'sql' => 'text DEFAULT \'\' NOT NULL',
          'recherche' => '',
          'saisie' => '',
          'explication' => '',
          'saisie_options' => '',
        ),
        2 => 
        array (
          'nom' => 'Identifiant ITIS du taxon',
          'champ' => 'tsn',
          'sql' => 'bigint(21) NOT NULL',
          'caracteristiques' => 
          array (
            0 => 'obligatoire',
          ),
          'recherche' => '',
          'saisie' => '',
          'explication' => '',
          'saisie_options' => '',
        ),
        3 => 
        array (
          'nom' => 'Login de l\'observateur',
          'champ' => 'login_observateur',
          'sql' => 'varchar(255) DEFAULT \'\' NOT NULL',
          'recherche' => '',
          'saisie' => '',
          'explication' => '',
          'saisie_options' => '',
        ),
        4 => 
        array (
          'nom' => 'Nom de l\'observateur',
          'champ' => 'nom_observateur',
          'sql' => 'text DEFAULT \'\' NOT NULL',
          'recherche' => '',
          'saisie' => '',
          'explication' => '',
          'saisie_options' => '',
        ),
        5 => 
        array (
          'nom' => 'Id de l\'observateur assimilé à un auteur',
          'champ' => 'id_auteur',
          'sql' => 'bigint(21) NOT NULL default 0',
          'caracteristiques' => 
          array (
            0 => 'editable',
          ),
          'recherche' => '',
          'saisie' => '',
          'explication' => '',
          'saisie_options' => '',
        ),
        6 => 
        array (
          'nom' => 'Latitude du lieu d\'observation',
          'champ' => 'latitude_lieu',
          'sql' => 'double NULL NULL',
          'recherche' => '',
          'saisie' => '',
          'explication' => '',
          'saisie_options' => '',
        ),
        7 => 
        array (
          'nom' => 'Longitude du lieu d\'observation',
          'champ' => 'longitude_lieu',
          'sql' => 'double NULL NULL',
          'recherche' => '',
          'saisie' => '',
          'explication' => '',
          'saisie_options' => '',
        ),
        8 => 
        array (
          'nom' => 'Id GIS du lieu',
          'champ' => 'id_lieu',
          'sql' => 'bigint(21) NOT NULL default 0',
          'caracteristiques' => 
          array (
            0 => 'editable',
          ),
          'recherche' => '',
          'saisie' => '',
          'explication' => '',
          'saisie_options' => '',
        ),
        9 => 
        array (
          'nom' => 'Nom du lieu',
          'champ' => 'nom_lieu',
          'sql' => 'text DEFAULT \'\' NOT NULL',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '2',
          'saisie' => '',
          'explication' => '',
          'saisie_options' => '',
        ),
        10 => 
        array (
          'nom' => 'Date de l\'observation',
          'champ' => 'date_observation',
          'sql' => 'datetime DEFAULT \'0000-00-00 00:00:00\' NOT NULL',
          'caracteristiques' => 
          array (
            0 => 'obligatoire',
          ),
          'recherche' => '',
          'saisie' => '',
          'explication' => '',
          'saisie_options' => '',
        ),
        11 => 
        array (
          'nom' => 'Date d\'état',
          'champ' => 'date',
          'sql' => 'datetime DEFAULT \'0000-00-00 00:00:00\' NOT NULL',
          'caracteristiques' => 
          array (
            0 => 'editable',
          ),
          'recherche' => '',
          'saisie' => '',
          'explication' => '',
          'saisie_options' => '',
        ),
      ),
      'champ_titre' => '',
      'champ_date' => 'date',
      'champ_date_ignore' => '',
      'statut' => 'on',
      'chaines' => 
      array (
        'titre_objets' => 'Observations de taxons',
        'titre_page_objets' => 'Les observations de taxons',
        'titre_objet' => 'Observation de taxon',
        'info_aucun_objet' => 'Aucune observation de taxon',
        'info_1_objet' => 'Une observation de taxon',
        'info_nb_objets' => '@nb@ observations de taxons',
        'icone_creer_objet' => 'Créer une observation de taxon',
        'icone_modifier_objet' => 'Modifier cette observation de taxon',
        'titre_logo_objet' => 'Logo de cette observation de taxon',
        'titre_langue_objet' => 'Langue de cette observation de taxon',
        'texte_definir_comme_traduction_objet' => 'Cette observation de taxon est une traduction de la observation de taxon numéro :',
        'titre_\\objets_lies_objet' => 'Liés à cette observation de taxon',
        'titre_objets_rubrique' => 'Observations de taxons de la rubrique',
        'info_objets_auteur' => 'Les observations de taxons de cet auteur',
        'retirer_lien_objet' => 'Retirer cette observation de taxon',
        'retirer_tous_liens_objets' => 'Retirer toutes les observations de taxons',
        'ajouter_lien_objet' => 'Ajouter cette observation de taxon',
        'texte_ajouter_objet' => 'Ajouter une observation de taxon',
        'texte_creer_associer_objet' => 'Créer et associer une observation de taxon',
        'texte_changer_statut_objet' => 'Cette observation de taxon est :',
        'supprimer_objet' => 'Supprimer cette observation de taxon',
        'confirmer_supprimer_objet' => 'Confirmez-vous la suppression de cette observation de taxon ?',
      ),
      'liaison_directe' => '',
      'table_liens' => '',
      'vue_liens' => 
      array (
        0 => 'spip_taxons',
      ),
      'afficher_liens' => '',
      'roles' => '',
      'auteurs_liens' => '',
      'vue_auteurs_liens' => 'on',
      'fichiers' => 
      array (
        'echafaudages' => 
        array (
          0 => 'prive/squelettes/contenu/objets.html',
          1 => 'prive/objets/infos/objet.html',
          2 => 'prive/squelettes/contenu/objet.html',
        ),
      ),
      'saisies' => 
      array (
        0 => 'objets',
      ),
      'autorisations' => 
      array (
        'objets_voir' => '',
        'objet_creer' => 'jamais',
        'objet_voir' => '',
        'objet_modifier' => 'administrateur',
        'objet_supprimer' => 'jamais',
        'associerobjet' => '',
      ),
      'boutons' => 
      array (
        0 => 'menu_edition',
      ),
    ),
  ),
  'images' => 
  array (
    'paquet' => 
    array (
      'logo' => 
      array (
        0 => 
        array (
          'extension' => 'svg',
          'contenu' => 'PHN2ZyBoZWlnaHQ9IjY1LjA0IiB2aWV3Qm94PSIwIDAgMjQgMjQiIHdpZHRoPSI2NS4wNCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJtMTguMDQ3IDEyYzAgMy4zNC0yLjcwNyA2LjA0Ny02LjA0NyA2LjA0N3MtNi4wNDctMi43MDctNi4wNDctNi4wNDcgMi43MDctNi4wNDcgNi4wNDctNi4wNDcgNi4wNDcgMi43MDcgNi4wNDcgNi4wNDd6Ii8+PHBhdGggZD0ibTEyIDRjLTkgMC0xMiA4LTEyIDhzMyA4IDEyIDggMTItOCAxMi04LTMtOC0xMi04em0wIDE0Yy02LjEgMC04LjktNC4zLTkuOC02IC45LTEuNyAzLjgtNiA5LjgtNiA2LjEgMCA4LjkgNC4zIDkuOCA2LS45IDEuNy0zLjggNi05LjggNnoiIGZpbGw9IiNkNzRhN2QiLz48cGF0aCBkPSJtNy45MTggMTEuMzM1Yy0uMDQzLS4wNTYtLjE4Mi0uMjg0LS4xODItLjY3NSAwLS43NjEuNTEyLTEuMjc3IDEuMDMzLTEuMjc3LjI0NiAwIC4zNjguMDQyLjQxOC4wNTMuMTE4LS4xNDIgMS41MzctMS45NjcgMy44NjktMS45NjcgMS45NCAwIDMuOTQ0LjggMy45NDQgMy41OTEgMCAxLjUwNS0uOTM4IDIuNTIxLTEuNTg0IDIuNzY1LS4yNzkuMTA1LS4zMi4zMDItLjMyLjQ2IDAgLjE2LjAyMS41MjEuMDU5LjguMDE5LjEzNy4yMzIuMjE5LjM2OS40MTQuMTM4LjE5NC4yMTIuMzIzLjIzNi41MjIuMDE5LjE1MyAwIC4xOTQtLjExLjA1LS4xMTEtLjE0Ni0uMzM5LS41MDYtLjg0OS0uNTA2LS4wNDEgMC0uMTQ2LS4wMzItLjI4My4xMDUtLjEzOC4xMzgtLjMzMi4zNjYtLjQwMS4zODktLjA5NC4wMzItLjI5Ny4yMTgtLjM3Ny4zNDMtLjA4LjEyNi0uMTI2LjE5OC0uMTI2LjAzNSAwLS4xNjEuMDA3LS4yODQuMTk0LS41MjYuMTg5LS4yNDUuMzA5LS4zODcuMzQzLS40MjQtLjI5Ni4wOTEtLjUyNC4wOC0uODkyLjI5OC0uMTI4LjA3Ni0uMTU2LjA1Mi0uMDQ5LS4wOTQuMTE5LS4xNjMuMjM4LS4yNjYuNTMtLjM4Ny4yNzUtLjExMy42NDgtLjE1OS43MzUtLjE3Mi4wOC0uMDEzLjE2Ny0uMTE2LjE4Ny0uMzEyLjAxNi0uMTQ4LjA1Mi0uNjE0LS4wMTktLjc4NS0uMDQ0LS4xMDgtLjM0NS0uMTU5LS42MTctLjc0NC0uMTE4LjA3Mi0uMzg0LjE4MS0uNjEyLjI0LS4xMDYuMDI4LS4xMzEuMzg0LS4xNDMuNjQxLS4wMTMuMjYzLjAzNC42NC4wNDYuNzQzLjAxMS4xMDMuMDMzLjE2MS0uMDg3LjIxOS0uMzI0LjE1Ni0uMzA4LjI1Ni0uNTAyLjQ1My0uMTY0LjE2NS0uMjIyLjI5NC0uMjQ4LjM1OC0uMDQzLjEwNC0uMDY3LjA0NC0uMDY2LS4wNS4wMDItLjE1OC4wMjYtLjM2Mi4xMTUtLjUyNC0uMjA5LjA5MS0uNDU3LjE5OC0uNjI5LjI0My0uMTcxLjA0Ni0uMzY3LjAxNy0uNTguMDkzLS4xNzguMDYzLS4xODQuMDgtLjI3Ni4xMTQtLjA5MS4wMzUtLjA0OS0uMDQyLjA2Mi0uMTQ4LjA5OS0uMDk2LjQ1OC0uMjc2LjY1Mi0uMzM0LjE5NS0uMDU3LjQzMS0uMTkxLjQ3NC0uMjE2LS4wNjMtLjA3NC0uMjU2LS4xMTQtLjQyOC0uMTE0LS4xMDEgMC0uMjEyLjAxLS4zMTUuMDIyLS4wODguMDA5LS4xLS4wMjIuMDQ0LS4xMTcuMTU2LS4xMDMuNDgxLS4xLjY0Mi0uMS4xNiAwIC4zMjMuMDA5LjQzOS4wMDkuMDg5IDAgLjE3OC0uMDgyLjIxMS0uMy4wMjctLjE3Ny4wNjMtLjY2LjA2My0uOTAxLS4xOTYtLjEzLS43NzUtLjM2NC0uNzc1LTEuNDIzLS4yNi0uNTA5LS41OTMtLjYwMi0uOTM5LS42MDItLjUxMiAwLS43MjYuNDI5LTEuODUzLjA1OS0uMjE2LS4wNzEtLjQyOS0uMDMzLS43MTguMjEzLS40NjEuMzkyLTEuMDM2IDEuMzM0LTEuMjY1IDIuMTYxLS4xMzYuNDkyLS4yMDcuOTgyLS4yNy45ODItLjA5MiAwLS4wNzQtLjQxMy0uMDQ0LS42MjYuMTYtMS4xNC41NTItMi4zMTQuODk0LTMuMDUxeiIgZmlsbD0iI2M1YmZjMSIvPjwvc3ZnPg==',
        ),
      ),
    ),
    'objets' => 
    array (
      0 => 
      array (
        'logo' => 
        array (
          0 => 
          array (
            'extension' => 'png',
            'contenu' => 'iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAHhlWElmTU0AKgAAAAgABAEaAAUAAAABAAAAPgEbAAUAAAABAAAARgEoAAMAAAABAAIAAIdpAAQAAAABAAAATgAAAAAAAABIAAAAAQAAAEgAAAABAAOgAQADAAAAAQABAACgAgAEAAAAAQAAAECgAwAEAAAAAQAAAEAAAAAAdd52hwAAAAlwSFlzAAALEwAACxMBAJqcGAAABm9JREFUeAHtmVlsFlUUx1tQa8viiiwSSERZ4vIgKiKaguIDYrTGJYrBWE2I4otrQIILKjHxwS0qSjQqGhNc0MQoQRMFKRoXBMVISSCtGxUBtbYFi1r8/ZtvcDq9986d6Xxfmzgn+TEz9yxzz/nuNqWsLJe8AnkF8grkFcgrkFcgr0Begf9nBcpLnPZw3ieGwACogA5oh1b4BbbDLiiJFLMAI8hgBkyGiTAWqsBHmjHaDOuhDlbBb9DnZRw9fAA2wP4M+ZtYa2EeqLB9SvrTmxp4HzScs0zcFOsv3vEqVEOvy4X04FswdbQUbRoVk3qjCifx0g8zSHwfMTS8e1qs5cRINTWSLoL9eNGtoHmuFdxHvsdoHXwGW2Eb/AxtoAJIFGsgKInj4AQ4A6aAb2K/YjsXVIyiiLYu319di+B8OD6Dnkwgxl3wDfiMlJewq4RM5WSiNYCrA/+gV/XPgmJJNYFXQNxiq9Gm80YmMo0oLeBK/h30KlJaGYzjKBgKh3oEORUb7TquPv2AXttyj+Q8vPeA7UVN6C5N8Yb++FwGGq5N5eXl+8PQ1gCvwyyoAptIvxNc/dMUSiXn4LUXbMH1qx+VIvJsfBrCCbvusdWCeS3YZBiKD8DWT/lrYU0kGjq7wRZ0EbqkO8jR+KxyJevS4fuE450aUY+Arb9b0B0JXnI4VtquTMG0Z9d6RelqpO+ARleCPjpiLO4attvTLbSY+q221aBCxcqbWJiCKPkrY727GhzMo4Zft3nuk3DUhjjqw3hwyU0oTf1X24MuR+lcztfHOYf0OsRsUQJc26OJ9OSZeJoKcbIQA1MRtH1OtzmPQdFqcbzf5mRo1yHku54k6fJVbMM7TU3P02gqgvwHmRz0zW1y0GqfZMGrcSWQhY7++CxoOk98acnpUdq7yCU8mZL/iXat3klkThZJumLQGd9Dl3azNojmprVEH3SdchD/1kPUSM8zOy2S/TPF1fksdHRHf2HylZsxNOX2dhDgGouBzvVpZCRJdgSJEmBPcB9cadM8bA6ek17xTXKw6Ye9bSqcjq7sa4hW6E/adDZPI/cpIRx1+DgFtH7o7K5CXwXHgkRb5OIUyf+Bn9d+rpcUpJprNEc9d/7IawxKfdmp82lkG047YKSn86YkRSDma55xw2aX82AqwNMyusCiXC1lQjmt8OtrUfWVNxIW4GzfwAU7fUxpykULoIXwwN8rPjYYyKEWksjdGK9M4oDtdb4FwHZZwtgyt30jPBeOpT9idEC0Sr/TNjpsGHO/Fr1OgUlE87lzFHDVX3zfhZXRotD2EQyAJHIuxprO0bx04Os2RV80GMpRo0MLVpxoO/00zsihr0N3W0Gvs0d9YTqpszqJVhR0vpfhGG6HaPJ6nm8Kom97fTubHDoXC5NTqE0HiwWh56S3n+BQE3JSETRPZ4TafG91CrRNa22J+rGMcjGtpgKobZ7R479GrbSaSmlFC1V08Wyh7cCJzTOw9v0VYMpD23vsKfIxi7MC6mvRJtpNDrEpDe0n0nYPLIEXCsN9TshuEG1al6aG2uJutZ68DKbk1XZDXADpNd9tw0dB7pBRBqLOqmj1oKGujzHN26nwMGwsFGUp9+UQJxr2OiPYkl8WFyCsH8LDVkewJ9H5LIzhmOH7yTwoyYdA0+YVqIQ7YSFsAP0IOsdrW70dXDIU5TqwJb8GXdJFtPN/Z2yLol6kDo6CNBJdhEYXgmhU1MJ7EP5mVyFsMg1FE9iS34TuCJtzXPsEDFzBm9HfCD5DNO5dgd433mAcngLT+SUohr5zNJp7JOPwboQgqOn6BfrpUArR1JsLrtGpPn4O2tozkWOI4ppjQVHqsLsCokM8i05oGGstaITgfbbrcmwqIVOpINozYHtpuH0Hdjo8nQ9VkFZ0GJoF+kv1Xgi/w3Svo/QC8J6S3oYEDWQmN8/CsKAh5qpObQRNE30qB5/LbdwLHVwGwEDQ3wrGwFg4EzT9fGUzhrNhva9DT+w0t5aAkjP9EqVsa6UP94LOASWX8bzxLShlwsG7VPyloMNTr4sK8ThoWww6WKxrE+9YBCOgz4nm8dWgk91uyKoIPxJLv/ZFoG0wM0mzCPq+XKe6SaBj70TQgqYF7jBwyS6UOoZrUdOCpq31KyiKFLMAtg5rf9bpTCu/tlaNknZogZ2wD3LJK5BXIK9AXoG8AnkF8grkFcgrUOQK/At5uxTPLjqrtwAAAABJRU5ErkJggg==',
          ),
          32 => 
          array (
            'extension' => 'png',
            'contenu' => 'iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAHhlWElmTU0AKgAAAAgABAEaAAUAAAABAAAAPgEbAAUAAAABAAAARgEoAAMAAAABAAIAAIdpAAQAAAABAAAATgAAAAAAAABIAAAAAQAAAEgAAAABAAOgAQADAAAAAQABAACgAgAEAAAAAQAAACCgAwAEAAAAAQAAACAAAAAAnwlWxAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAvBJREFUWAntlluITWEUx+eM+23cRmMSZ1JKiCiOKKGQRB7mgWRyGS8e5FbIbQbxMk9CXiRJknihSE3N8CJpoobEk2IekMs0LmMax+9/Omtmnd3e2zmTkjqrfvOt2/d9a397fftMSUlRiifwj08gUeD+SfJTMB3GQxmkoR3aoBUeZnWGvyPlLLMftLg2y4cW8nbCSOizDGPmcfgKUZv+iolpzmfYBwOhIJlH9ivwG3dgX4UtMAt0/HqFYhTMhlq4BsGin+LTa8tL1pH1A2zzj+hHYTSEiQoYDqUuOBb9GHwCW0d9shJipYZoN9ikW+gVETPW4m+EzkQiofzv0AzVYFKJchdsvU701RYMjstwdIElHwkmZO0hjNe1aRTET7i5OqGTYOt+Q5/r4hl1An8/gCXtCiZg65j3QlPUxs6v5gy+88P4bP3X6Dmv9LYLnkYPkwa3QeTTWw4LHAhZ5BI+K+KixVc5p+7vAAsExiu2eD4jc88G5sscCv52pfrjqFcEUWXbQH0QJol0Ov2AwCOoglZIUswmxjD5EuLU+98O97KxOhWg7pUkQBWGyQicVbAUdEV7hKKWUESyx9Gr3OhVczStZZLZezGWvZfn6IMt6kZduYXO9mo9BajpXjJmegP9nE9w+hj0N6D9dN1nQkb0hbMiLmR9ftjojRD9DD6dpo52B+g0g9IPR2SzlxO0ylTIocDsVMA2cyrKGtADSOpAVzooKug82EO+QNdvTY7Mx9I7saRT6GFPYpPKUNbDTagGfWzUpLvBi36ILoOtqx+oaT7B6/pM6nNpyY3oE31CiD4o69vMqGP2JzAD+wnYeh3oiyBWVhDVFfKTGrArY2flBqdgqpd+gq3zDn0B5CV6t4/BJmvUYndgD+g6Tgb1zjjQhsvhINyHbvBzm7EnQUGirlZHvwe/WCH6W+ZuhVLos+jjVAtN0AV/KkA9pOu4AdSAsRLX5WET9T/eHFBzVYBsfYTaoQ2eQQuo2YpSPIH/4wR+AxTBC2njnMmSAAAAAElFTkSuQmCC',
          ),
          24 => 
          array (
            'extension' => 'png',
            'contenu' => 'iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAHhlWElmTU0AKgAAAAgABAEaAAUAAAABAAAAPgEbAAUAAAABAAAARgEoAAMAAAABAAIAAIdpAAQAAAABAAAATgAAAAAAAABIAAAAAQAAAEgAAAABAAOgAQADAAAAAQABAACgAgAEAAAAAQAAABigAwAEAAAAAQAAABgAAAAA0bpBmgAAAAlwSFlzAAALEwAACxMBAJqcGAAAAeBJREFUSA3tlL9LHEEUx++MgomNipWSVEHSBCNCwB9YBjwECcHg/5Eqgcg16ZXD1iaQPqKIYKtCLEwV0EKOxEYIQVKE2Nz6+Sw3d7PHwikoNveFz86bN2/f7r6dN4VCR/ddgWKbF5hifQEmYRT6QV3ACRzAV9iHG6lE9HdIromx3tNWj4j4DCHxb+wKzMMI9NQZZjThKpxDiPdec+RqAO8hGPwfPkAfxLJk76EMb8FkD+Ed/APv/QbmyqiXmXU04Cc8h1hDTHaKxWISg68KT0E9g1Mwh7nM2dAalgu/4HHDWyh0YZfhR5w4tlnbgCBLVwVzmTPVDNcaXMJ46mleXsbJ8mxC/zbDU2uMqyU257SeUJplJy2aYH4Gm7ALW60PwfcHWvURR1qq7mhFR6te4ZiF07CQJMkxDxkNc8btyA5mpr9CifwsPy/W63hSt5cY1+EI9sANEMsyW+4apCVyMfzkKrY/KuhFMOrjIOMizIHb9AnEcoO4UTI/2YB4m1oOt1yefKNPYON9gTcQ5NZ2i6e1Z8xsU4NsjtBoNo3NYxPlya5+UMdmtCktsclzGw1/KjvTdjdQPAZWoASWLj4q/IoKeJyEeO81R1uZ8FYOu8x2ynnsFL47Oa5zntVx3VEFrgBEopQFG1OLLAAAAABJRU5ErkJggg==',
          ),
          16 => 
          array (
            'extension' => 'png',
            'contenu' => 'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAHhlWElmTU0AKgAAAAgABAEaAAUAAAABAAAAPgEbAAUAAAABAAAARgEoAAMAAAABAAIAAIdpAAQAAAABAAAATgAAAAAAAABIAAAAAQAAAEgAAAABAAOgAQADAAAAAQABAACgAgAEAAAAAQAAABCgAwAEAAAAAQAAABAAAAAAB9pFxQAAAAlwSFlzAAALEwAACxMBAJqcGAAAATtJREFUOBHd0j9IAmEcxvG7LDRQWiIMxGgQLKIlozZbGsIlbGloaBR0aGt3KqiptaFahHBrKIRA15aGhIYgaFBQIYpM6A/W97l84aJyFXrgc+/7/u7e9z3f07J6HfuPFxihPgu1b7jFBd7RNXPcPYMebOMeLXygjh0M4ddkqWrSKZYwiAGEMY4M7lBFDN+yxegFa66q+g3btrX7I9bhxzGeMA0nCa7aeeVr6FzPmdjWZIOqFtGZ9eEEN9BbWtc4UqcTD20Rh8i7Fnhl3A9lGE1saJBDGV4NyAx0+op2uEQDuzBZpKOftqDCKGrQQto9CJMQnRTSpkA7CS2476pZ8wweUMCY64YWjyCOAFahT6vnnDfWoZhM0TmATleHVII+mQ9RLHfaPdpN6Ex+RCecRB4V6F/4jCtsYwL/LZ/YMEKzcA2x8AAAAABJRU5ErkJggg==',
          ),
          12 => 
          array (
            'extension' => 'png',
            'contenu' => 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAAAXNSR0IArs4c6QAAAHhlWElmTU0AKgAAAAgABAEaAAUAAAABAAAAPgEbAAUAAAABAAAARgEoAAMAAAABAAIAAIdpAAQAAAABAAAATgAAAAAAAABIAAAAAQAAAEgAAAABAAOgAQADAAAAAQABAACgAgAEAAAAAQAAAAygAwAEAAAAAQAAAAwAAAAAIIPOagAAAAlwSFlzAAALEwAACxMBAJqcGAAAAMNJREFUKBW9kD8OQUEQh2cTnUIkEgqVQk0j6leIxAE0HIBa6yASx1Bp3hHeCXSPkGg0olvfb3dJlBLxS743f3bnzc6Y/VM1mq2hgFtCvnI6+9CA6AhbGEITMhjBBkrQnaAu3wtMYmhT7NU557F36MEYzqC7lsNSDmrBCQ4qSEUrHaAF5JXgmulvUhs68PDe77B92IPkoolt1E5tqynZwM5hlmI9V88OT1JOA2kwDShfhXXQArQILeQ9NH7QV2t9Ff3ePgEp5iiD61MrcAAAAABJRU5ErkJggg==',
          ),
        ),
      ),
    ),
  ),
);
