<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_taxonview' => 'Ajouter cette observation de taxon',

	// C
	'champ_date_label' => 'Date d\'état',
	'champ_date_observation_label' => 'Date de l\'observation',
	'champ_id_auteur_label' => 'Id de l\'observateur assimilé à un auteur',
	'champ_id_lieu_label' => 'Id GIS du lieu',
	'champ_latitude_lieu_label' => 'Latitude du lieu d\'observation',
	'champ_login_observateur_label' => 'Login de l\'observateur',
	'champ_longitude_lieu_label' => 'Longitude du lieu d\'observation',
	'champ_nom_lieu_label' => 'Nom du lieu',
	'champ_nom_observateur_label' => 'Nom de l\'observateur',
	'champ_tsn_label' => 'Identifiant ITIS du taxon',
	'champ_uid_naturaliste_label' => 'Identifiant iNaturalist',
	'champ_url_naturaliste_label' => 'URL iNaturalist de l\'observation',
	'confirmer_supprimer_taxonview' => 'Confirmez-vous la suppression de cette observation de taxon ?',

	// I
	'icone_creer_taxonview' => 'Créer une observation de taxon',
	'icone_modifier_taxonview' => 'Modifier cette observation de taxon',
	'info_1_taxonview' => 'Une observation de taxon',
	'info_aucun_taxonview' => 'Aucune observation de taxon',
	'info_nb_taxonviews' => '@nb@ observations de taxons',
	'info_taxonviews_auteur' => 'Les observations de taxons de cet auteur',

	// R
	'retirer_lien_taxonview' => 'Retirer cette observation de taxon',
	'retirer_tous_liens_taxonviews' => 'Retirer toutes les observations de taxons',

	// S
	'supprimer_taxonview' => 'Supprimer cette observation de taxon',

	// T
	'texte_ajouter_taxonview' => 'Ajouter une observation de taxon',
	'texte_changer_statut_taxonview' => 'Cette observation de taxon est :',
	'texte_creer_associer_taxonview' => 'Créer et associer une observation de taxon',
	'texte_definir_comme_traduction_taxonview' => 'Cette observation de taxon est une traduction de la observation de taxon numéro :',
	'titre_langue_taxonview' => 'Langue de cette observation de taxon',
	'titre_logo_taxonview' => 'Logo de cette observation de taxon',
	'titre_objets_lies_taxonview' => 'Liés à cette observation de taxon',
	'titre_page_taxonviews' => 'Les observations de taxons',
	'titre_taxonview' => 'Observation de taxon',
	'titre_taxonviews' => 'Observations de taxons',
	'titre_taxonviews_rubrique' => 'Observations de taxons de la rubrique',
);
