<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'naturaliste_description' => 'Ce plugin permet de récupérer les observations d\'espèces du monde vivant enregistrées sur la plate-forme iNaturalist et de lier ces observations aux taxons fournis par le plugin Taxonomie.',
	'naturaliste_nom' => 'Naturaliste',
	'naturaliste_slogan' => 'Observer le monde vivant',
);
