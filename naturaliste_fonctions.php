<?php
/**
 * Fonctions utiles au plugin Naturaliste
 *
 * @plugin     Naturaliste
 * @copyright  2020
 * @author     Eric Lupinacci
 * @licence    GNU/GPL
 * @package    SPIP\Naturaliste\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
